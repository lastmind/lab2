﻿using System;
using System.Collections.Generic;
using System.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace Danger_list
{
    class Element
    {
        private int id;
        private string name;
        private string explanation;
        private string source;
        private string obj;
        private bool konf;
        private bool cel;
        private bool dost;
        private DateTime date1;
        private DateTime date2;

        public Element(Excel.Range range) 
        {
            this.id = (int)range.Value2;
            range = range.Offset[0, 1];
            this.name = range.Value2.ToString();
            range = range.Offset[0, 1];
            this.explanation = range.Value2.ToString();
            range = range.Offset[0, 1];
            this.source = range.Value2.ToString();
            range = range.Offset[0, 1];
            this.obj = range.Value2.ToString();
            range = range.Offset[0, 1];
            this.konf = range.Value2 == 1;
            range = range.Offset[0, 1];
            this.cel = range.Value2 == 1;
            range = range.Offset[0, 1];
            this.dost = range.Value2 == 1;
            range = range.Offset[0, 1];
            this.date1 = DateTime.FromOADate(range.Value2);
            range = range.Offset[0, 1];
            this.date2 = DateTime.FromOADate(range.Value2);
        }


        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Explanation { get => explanation; set => explanation = value; }
        public string Source { get => source; set => source = value; }
        public string Obj { get => obj; set => obj = value; }
        public bool Konf { get => konf; set => konf = value; }
        public bool Cel { get => cel; set => cel = value; }
        public bool Dost { get => dost; set => dost = value; }
        public DateTime Date1 { get => date1; set => date1 = value; }
        public DateTime Date2 { get => date2; set => date2 = value; }
        public string Date1s { get => date1.ToShortDateString(); }
        public string Date2s { get => date2.ToShortDateString(); }
        public string Konfs { get => konf ? "Да" : "Нет"; }
        public string Cels { get => cel ? "Да" : "Нет"; }
        public string Dosts { get => dost ? "Да" : "Нет"; }
        public string Ids { get => "УБИ." + "000".Substring(0,3-id.ToString().Length)+id.ToString(); }

        public override bool Equals(object obj)
        {
            return obj is Element element &&
                   Id == element.Id &&
                   Name == element.Name &&
                   Explanation == element.Explanation &&
                   Source == element.Source &&
                   Obj == element.Obj &&
                   Konf == element.Konf &&
                   Cel == element.Cel &&
                   Dost == element.Dost &&
                   Date1 == element.Date1 &&
                   Date2 == element.Date2;
        }

        public static bool operator ==(Element left, Element right)
        {
            return EqualityComparer<Element>.Default.Equals(left, right);
        }

        public static bool operator !=(Element left, Element right)
        {
            return !(left == right);
        }

        public bool Find(string s)
        {
            return id.ToString().Contains(s) ||
                   name.Contains(s) ||
                   explanation.Contains(s) ||
                   source.Contains(s) ||
                   obj.Contains(s) ||
                   Date1s.Contains(s) ||
                   Date2s.Contains(s);
        }

        public override int GetHashCode()
        {
            var hashCode = 1763247094;
            hashCode = hashCode * -1521134295 + id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(explanation);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(source);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(obj);
            hashCode = hashCode * -1521134295 + konf.GetHashCode();
            hashCode = hashCode * -1521134295 + cel.GetHashCode();
            hashCode = hashCode * -1521134295 + dost.GetHashCode();
            hashCode = hashCode * -1521134295 + date1.GetHashCode();
            hashCode = hashCode * -1521134295 + date2.GetHashCode();
            return hashCode;
        }
    }
}
