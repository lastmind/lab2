﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Excel = Microsoft.Office.Interop.Excel;

namespace Danger_list
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static private bool fileUpdated = false;
        static private Excel.Application app;
        static private List<Element> showOld = new List<Element>();
        static private List<Element> showNew = new List<Element>();
        static private List<Element> show = new List<Element>();
        static private Element currentElement;
        static private ObservableCollection<Element> showPage = new ObservableCollection<Element>();
        const int elementsPerPage = 20;
        static private int maxPage = 1;
        static private int maxNewPage = 1;
        Timer blinker;
        public MainWindow()
        {
            InitializeComponent();
        }
        public void CloseExcel(object sender, EventArgs e)
        {
            app.Quit();
            if (File.Exists(@"~saved_file.xlsx")) File.Delete(@"~saved_file.xlsx");
            if (File.Exists(@"~saved_file.xlsx.backup")) File.Delete(@"~saved_file.xlsx.backup");
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            app = new Excel.Application();
            Application.Current.Exit += CloseExcel;
            try
            {
                app.Workbooks.Open(AppContext.BaseDirectory + @"\saved_file.xlsx",
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing);
            }
            catch (Exception)
            {
                var ans = MessageBox.Show("Кажется, база данных еще не загружена.\n" +
                    "Хотите загрузить?",
                    "Загрузить базу данных?",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (ans == MessageBoxResult.Yes)
                {
                    if (!DownloadFile(false))
                    {
                        Application.Current.Shutdown();
                    }
                    MessageBox.Show("Загружена последняя версия файла! :)");
                    try
                    {
                        app.Workbooks.Open(AppContext.BaseDirectory + @"\saved_file.xlsx",
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Type.Missing);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Произошла неизвестная ошибка. Просим прощения за неудобства.",
                                        "Ошибка",
                                        MessageBoxButton.OK,
                                        MessageBoxImage.Error);
                        Application.Current.Shutdown();
                        return;
                    }
                    fileUpdated = true;
                }
                else
                {
                    MessageBox.Show("Штош...");
                    Application.Current.Shutdown();
                    return;
                }
            }



            Excel.Worksheet sheet = app.Worksheets.Item[1];
            for (Excel.Range cell = sheet.Range["A3", Type.Missing]; cell.Value2 != null; cell = cell.Offset[1, 0])
            {
                showOld.Add(new Element(cell));
            }

            ChangeActualityStatus(ActualityStatus.All);
            app.Workbooks.Close();

            maxPage = (showOld.Count - 1) / elementsPerPage + 1;

            new Thread(CheckFile).Start();
        }

        private void CheckFile()
        {
            if (fileUpdated) return;

            var hashFunс = new MD5CryptoServiceProvider();
            DownloadFile();
            byte[] hash;
            byte[] hash_t;
            try
            {
                using (var buf = File.OpenRead(@"saved_file.xlsx"))
                using (var buf_t = File.OpenRead(@"~saved_file.xlsx"))
                {
                    hash = hashFunс.ComputeHash(buf);
                    hash_t = hashFunс.ComputeHash(buf_t);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Не удалось проверить актуальность данных, попробуйте позже.",
                    "Ошибка",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
            bool diff = false;
            for (int i = 0; i < 16; i++)
            {
                if (hash[i] != hash_t[i]) { diff = true; break; }
            }
            if (diff)
            {
                blinker = new Timer(Blink, null, 0, 500);
            }
        }

        private bool DownloadFile(bool temp = true)
        {
            using (var web = new WebClient())
            {
                try
                {
                    web.DownloadFile("https://bdu.fstec.ru/files/documents/thrlist.xlsx", temp ? @"~saved_file.xlsx" : @"saved_file.xlsx");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show("Не удается загрузить файл, попробуйте позже.",
                        "Ошибка",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return false;
                }
                return true;
            }
        }

        private void Find(object sender, RoutedEventArgs e)
        {
            if (Finder.Text == "") return;
            DataGrid curr;
            int next;
            if (DocumentAll.Visibility == Visibility.Visible) curr = DocumentAll;
            else
                curr = DocumentShort;
            if (curr.SelectedCells.Count == 0) next = -1;
            else
            {
                var currItem = (Element)curr.SelectedCells[0].Item;
                next = show.FindIndex((Element t) => t == currItem);
                if (next == show.Count - 1) { FindEOF(); return; }
            }
            int index = show.FindIndex(next + 1, (Element t) => t.Find(Finder.Text));
            if (index == -1) { FindEOF(); return; }
            PageNumber.Text = (index / elementsPerPage + 1).ToString();
            Pagination();
            curr.SelectedIndex = index % elementsPerPage;
        }
        private void FindEOF()
        {
            MessageBox.Show("Конец списка. Данные не найдены.");
        }

        private void CurrentVisibility_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            VisibleStatus t = VisibleStatus.All;
            switch ((e.AddedItems[0] as ComboBoxItem).Name)
            {
                case ("All"):
                    t = VisibleStatus.All;
                    break;
                case ("Short"):
                    t = VisibleStatus.Short;
                    break;
            }
            ChangeVisibleStatus(t);
        }

        private void ChangeActualityStatus(ActualityStatus newStatus)
        {
            switch (newStatus)
            {
                case ActualityStatus.All:
                    show = showOld;
                    break;
                case ActualityStatus.New:
                    show = showNew;
                    if (int.Parse(this.PageNumber.Text) > maxNewPage) this.PageNumber.Text = maxNewPage.ToString();
                    break;
            }
            Pagination();
        }
        private void ChangeVisibleStatus(VisibleStatus newStatus)
        {
            try
            {
                switch (newStatus)
                {
                    case VisibleStatus.All:
                        DocumentAll.Visibility = Visibility.Visible;
                        DocumentShort.Visibility = Visibility.Hidden;
                        break;
                    case VisibleStatus.Short:
                        DocumentAll.Visibility = Visibility.Hidden;
                        DocumentShort.Visibility = Visibility.Visible;
                        break;
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        private void Pagination()
        {
            int page = (int.Parse(this.PageNumber.Text));
            if (show.Count != 0)
                showPage = new ObservableCollection<Element>(show.GetRange(
                    (page - 1) * elementsPerPage,
                    Math.Min(elementsPerPage, show.Count - (page - 1) * elementsPerPage)));
            else showPage.Clear();
            DocumentAll.ItemsSource = showPage;
            DocumentShort.ItemsSource = showPage;
        }
        private void New_Checked(object sender, RoutedEventArgs e)
        {
            ChangeActualityStatus(ActualityStatus.New);                                              //Тут есть баг. Иногда при первой смене цвета
            DocumentAll.RowBackground = System.Windows.Media.Brushes.Coral;                          //цвета инвертируются, но это исправляется при
            DocumentAll.AlternatingRowBackground = System.Windows.Media.Brushes.LightGreen;          //повторном щелчке. Это происходит не всегда.
            DocumentShort.RowBackground = System.Windows.Media.Brushes.Coral;
            DocumentShort.AlternatingRowBackground = System.Windows.Media.Brushes.LightGreen;
        }

        private void New_Unchecked(object sender, RoutedEventArgs e)
        {
            ChangeActualityStatus(ActualityStatus.All);
            DocumentAll.RowBackground = System.Windows.Media.Brushes.White;
            DocumentAll.AlternatingRowBackground = System.Windows.Media.Brushes.White;
            DocumentShort.RowBackground = System.Windows.Media.Brushes.White;
            DocumentShort.AlternatingRowBackground = System.Windows.Media.Brushes.White;
        }

        private void PgDown(object sender, RoutedEventArgs e)
        {
            if (this.PageNumber.Text != "1")
            {
                this.PageNumber.Text = (int.Parse(this.PageNumber.Text) - 1).ToString();
                Pagination();
            }
        }

        private void PgUp(object sender, RoutedEventArgs e)
        {
            if (int.Parse(this.PageNumber.Text) < ((show == showOld) ? maxPage : maxNewPage))
            {
                this.PageNumber.Text = (int.Parse(this.PageNumber.Text) + 1).ToString();
                Pagination();
            }
        }

        private void PageNumberChanged(object sender, RoutedEventArgs e)
        {
            int t;
            if (!(int.TryParse(this.PageNumber.Text, out t) && (t > 0) && (t <= ((show == showOld) ? maxPage : maxNewPage))))
                this.PageNumber.Text = "1";
            Pagination();
        }

        private void Finder_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.Finder.Text == "") this.FinderPlaceholder.Visibility = Visibility.Visible;
            else this.FinderPlaceholder.Visibility = Visibility.Hidden;
        }

        private void ShowPopup(object sender, RoutedEventArgs e)
        {
            this.Popup.Visibility = Visibility.Visible;
        }

        private void Popup_MouseLeave(object sender, MouseEventArgs e)
        {
            Popup.Visibility = Visibility.Hidden;
        }
        private void Blink(object state)
        {
            Dispatcher.Invoke(BlinkAction);
        }
        private void BlinkAction()
        {
            if (PopupButton.Visibility == Visibility.Hidden) PopupButton.Visibility = Visibility.Visible;
            if (PopupButton.Opacity == 1) PopupButton.Opacity = 0;
            else PopupButton.Opacity = 1;
        }

        private void Reload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                File.Replace(@"~saved_file.xlsx", @"saved_file.xlsx", @"~saved_file.xlsx.backup");
                app.Workbooks.Open(AppContext.BaseDirectory + @"\saved_file.xlsx",
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing);
            }
            catch (Exception)
            {
                MessageBox.Show("Не удалось обновить содержимое файла",
                                "Ошибка",
                                MessageBoxButton.OK,
                                MessageBoxImage.Error);
                return;
            }
            Excel.Worksheet sheet = app.Worksheets.Item[1];
            Excel.Range cell = sheet.Range["A3", Type.Missing];
            int changes = 0;
            for (int i = 0; (i < showOld.Count) && (cell.Value2 != null); i++)
            {
                Element tmp = new Element(cell);
                if (tmp != showOld[i])
                {
                    showNew.Add(showOld[i]);
                    showNew.Add(tmp);
                    showOld[i] = tmp;
                    changes++;
                }
                cell = cell.Offset[1, 0];
                if (cell.Value2 == null)
                {
                    showNew.AddRange(showOld.GetRange(i + 1, showOld.Count - i - 1));
                    showOld.RemoveRange(i + 1, showOld.Count - i - 1);
                    changes += showOld.Count - i - 1;
                }
            }
            while (cell.Value2 != null)
            {
                showOld.Add(new Element(cell));
                showNew.Add(new Element(cell));
                cell = cell.Offset[1, 0];
                changes++;
            }
            maxPage = (showOld.Count - 1) / elementsPerPage + 1;
            maxNewPage = (showNew.Count - 1) / elementsPerPage + 1;

            app.Workbooks.Close();
            blinker.Dispose();
            Pagination();
            Popup.Visibility = Visibility.Hidden;
            PopupButton.Visibility = Visibility.Hidden;
            New.Visibility = Visibility.Visible;
            MessageBox.Show($"Данные успешно обновлены! :)\nОбновлено {changes} записей.",
                            "Данные обновлены",
                            MessageBoxButton.OK,
                            MessageBoxImage.Information);
            fileUpdated = true;
        }

        private void PageNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) { this.Grid.Focus(); this.PageNumber.Focus(); }
        }

        private void Document_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if ((sender as DataGrid).SelectedCells.Count == 0) return;
            currentElement = (Element)(sender as DataGrid).SelectedCells[0].Item;
            DocumentListing.Visibility = Visibility.Hidden;
            DocumentOne.Visibility = Visibility.Visible;
            DocumentOne.DataContext = currentElement;
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            DocumentListing.Visibility = Visibility.Visible;
            DocumentOne.Visibility = Visibility.Hidden;
        }

        private void Finder_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Find(null, null);
        }
    }
    enum VisibleStatus
    {
        All,
        Short,
    }
    enum ActualityStatus
    {
        All,
        New
    }
}
